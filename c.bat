@setlocal
@set DEFSRC=src\myapp\ClassBuilderTest.java
@set SRCDIR=src
@set CLSDIR=classes
@if "%~1"=="" goto LABEL1
javac -sourcepath %SRCDIR% -d %CLSDIR% %*
@goto LABEL3
:LABEL1
@if "%DEFSRC%"=="" goto LABEL2
@call %0 %DEFSRC%
@goto LABEL3
:LABEL2
@echo usage:  %~nx0 ^<SourceFile^>
:LABEL3
@endlocal
