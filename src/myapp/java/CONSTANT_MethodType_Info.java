package myapp.java;

class CONSTANT_MethodType_Info
	extends CP_Info
{
	
	final int descriptor_index;
	
	CONSTANT_MethodType_Info(int _descriptor_index)
	{
		super(CP_Tag.CONSTANT_MethodType);
		descriptor_index = _descriptor_index;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_MethodType_Info c = (CONSTANT_MethodType_Info)o;
		return c.descriptor_index == descriptor_index;
	}
}