package myapp.java;

abstract
class CONSTANT_ref_Info
	extends CP_Info
{
	
	final
	int class_index;
	
	final
	int name_index;
	
	protected
	CONSTANT_ref_Info(CP_Tag _tag, int _class_index, int _name_index)
	{
		super(_tag);
		class_index = _class_index;
		name_index = _name_index;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_ref_Info c = (CONSTANT_ref_Info)o;
		return c.class_index == class_index &&  c.name_index == name_index;
	}	
}
