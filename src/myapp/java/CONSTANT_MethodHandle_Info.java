package myapp.java;

class CONSTANT_MethodHandle_Info
	extends CP_Info
{
	
	final
	int reference_kind;
	
	final
	int reference_index;
	
	CONSTANT_MethodHandle_Info(int _reference_kind, int _reference_index)
	{
		super(CP_Tag.CONSTANT_MethodHandle);
		reference_kind = _reference_kind;
		reference_index = _reference_index;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_MethodHandle_Info c = (CONSTANT_MethodHandle_Info)o;
		return c.reference_kind == reference_kind && c.reference_index == reference_index;
	}
}