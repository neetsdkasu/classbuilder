package myapp.java;

import java.io.DataOutputStream;;
import java.io.IOException;

import java.util.HashMap;

class CONSTANT_Utf8_Info
	extends CP_Info
{
	final
	String str;
	
	CONSTANT_Utf8_Info( String _str )
	{
		super( CP_Tag.CONSTANT_Utf8 );
		str = _str;
	}
	
	@Override
	void write( DataOutputStream dos , HashMap< CP_Info , Integer > pool ) throws IOException
	{
		super.write( dos , pool );
		dos.writeShort( (short)str.length() );
		byte[] buf = str.getBytes( "UTF-8" );
		dos.write( buf , 0 , buf.length );
		buf = null;
	}

	
	@Override
	public
	boolean equals( Object o )
	{
		if ( super.equals( o ) == false)
		{
			return false;
		}
		CONSTANT_Utf8_Info c = (CONSTANT_Utf8_Info)o;
		return str.equals( c.str );
	}
}
