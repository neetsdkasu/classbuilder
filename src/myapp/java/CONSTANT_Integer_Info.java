package myapp.java;

class CONSTANT_Integer_Info extends CP_Info
{
	final
	int value;
	
	CONSTANT_Integer_Info(int _value)
	{
		super(CP_Tag.CONSTANT_Integer);
		value = _value;
	}

	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_Integer_Info c = (CONSTANT_Integer_Info)o;
		return c.value == value;
	}
}