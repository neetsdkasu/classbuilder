package myapp.java;

public
enum ClassAccessFlag {
	ACC_PUBLIC     ( 0x0001 ),
	ACC_FINAL      ( 0x0010 ),
	ACC_SUPER      ( 0x0020 ),
	ACC_INTERFACE  ( 0x0200 ),
	ACC_ABSTRACT   ( 0x0400 ),
	ACC_SYNTHETIC  ( 0x1000 ),
	ACC_ANNOTATION ( 0x2000 ),
	ACC_ENUM       ( 0x4000 );
	
	final
	int flag;
	
	ClassAccessFlag( int _flag )
	{
		flag = _flag;
	}
	
	public static
	ClassAccessFlag[] with( ClassAccessFlag ... flags )
	{
		return flags;
	}

	public static
	int getFlagValue(  ClassAccessFlag ... flags )
	{
		int value = 0;
		for ( ClassAccessFlag caf : flags )
		{
			value |= caf.flag;
		}
		return value;
	}
}