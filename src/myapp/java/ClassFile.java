package myapp.java;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.HashMap;

public
class ClassFile
{

	
	static final 
	int MAGIC = 0xCAFEBABE;
	
	private
	int     minor_version = 0;
	
	private
	int     major_version = 45;
	
	private
	String  className = null;
	
	private 
	String superclass = null;
	
	private
	ClassAccessFlag[] flags = null;
	
	private
	String[] interfaces = null;
	
	public
	ClassFile( int _version, String _className, ClassAccessFlag[] _flags, String _superClassName , String[] _interfaceNames )
	{
		major_version = 44 + _version;
		className = _className;
		superclass = _superClassName;
		flags = Arrays.copyOf( _flags, _flags.length );
		interfaces = Arrays.copyOf( _interfaceNames, _interfaceNames.length );
		
	}
	
	private
	LinkedList< CP_Info > makeConstantList()
	{
		LinkedList< CP_Info > list = new LinkedList<>();
		CONSTANT_Utf8_Info utf8;
		CP_Info temp;
		
		list.add( utf8 = new CONSTANT_Utf8_Info( className ) );
		list.add( new CONSTANT_Class_Info( utf8 ) );
		
		if ( superclass != null )
		{
			list.add( utf8 = new CONSTANT_Utf8_Info( superclass ) );
			list.add( new CONSTANT_Class_Info( utf8 ) );
		}
		
		if ( interfaces != null )
		{
			for ( String name : interfaces )
			{
				list.add( utf8 = new CONSTANT_Utf8_Info( name ) );
				list.add( new CONSTANT_Class_Info( utf8 ) );
			}
		}
		
		// TODO field
		
		// TODO method
		
		return list;
	}
	
	public
	byte[] toByteArray()
	{
		byte[] buf = null;
		try ( ByteArrayOutputStream baos = new ByteArrayOutputStream() )
		{
			DataOutputStream dos = new DataOutputStream( baos );
			
			dos.writeInt( MAGIC );
			dos.writeShort( (short)minor_version );
			dos.writeShort( (short)major_version );
			
			HashMap< CP_Info , Integer > pool = new HashMap<>();
			
			{
				LinkedList< CP_Info > list = makeConstantList();
				
				dos.writeShort( (short)list.size() );
				
				for ( CP_Info cp : list )
				{
					cp.write( dos , pool );
					pool.put( cp , Integer.valueOf( pool.size() + 1 ) );
				}
			}
			
			dos.writeShort( (short)ClassAccessFlag.getFlagValue( flags ) );
			
			dos.writeShort( pool.get( className ).shortValue() );
			
			if ( superclass != null )
			{
				dos.writeShort( pool.get( superclass ).shortValue() );
			}
			else
			{
				dos.writeShort( 0 );
			}
			
			if ( interfaces != null )
			{
				dos.writeShort( (short)interfaces.length );
				for ( String name : interfaces )
				{
					dos.writeShort( pool.get( name ).shortValue() );
				}
			}
			else
			{
				dos.writeShort( 0 );
			}
			
			// TODO field
			
			// TODO method
			
			// TODO attribute
			
			buf = baos.toByteArray();
		}
		catch ( IOException ex )
		{
			return null;
		}
		return buf;
	}
	
}