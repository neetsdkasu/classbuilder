package myapp.java;

class CONSTANT_Long_Info
	extends CP_Info
{
	final
	long value;
	
	CONSTANT_Long_Info(long _value)
	{
		super(CP_Tag.CONSTANT_Long_Info);
		value = _value;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_Long_Info c = (CONSTANT_Long_Info)o;
		return c.value == value;
	}
}
}