package myapp.java;

class CONSTANT_InvokeDynamic_Info
	extends CP_Info
{
	
	final
	int bootstrap_method_attr_index;
	
	final
	int name_and_type_index;
	
	CONSTANT_InvokeDynamic_Info(int _bootstrap_method_attr_index, int _name_and_type_index)
	{
		super(CP_Tag.CONSTANT_InvokeDynamic);
		bootstrap_method_attr_index = _bootstrap_method_attr_index;
		name_and_type_index = _name_and_type_index;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_InvokeDynamic_Info c = (CONSTANT_InvokeDynamic_Info)o;
		return c.bootstrap_method_attr_index == bootstrap_method_attr_index && c.name_and_type_index == name_and_type_index;
	}
}