package myapp.java;

import java.io.DataOutputStream;;
import java.io.IOException;

import java.util.HashMap;

abstract
class CP_Info
{
	
	final
	CP_Tag tag;
	
	protected
	CP_Info( CP_Tag _tag )
	{
		tag = _tag;
	}
	
	void write( DataOutputStream dos , HashMap< CP_Info , Integer > pool ) throws IOException
	{
		dos.writeByte( (byte)tag.getNumber() );
	}

	@Override
	public
	boolean equals( Object o )
	{
		if ( o == this )
		{
			return true;
		}
		if ( o == null )
		{
			return false;
		}
		if ( o.getClass().equals( this.getClass() ) == false )  // ここ間違いくさい
		{
			return false;
		}
		CP_Info c = (CP_Info)o;
		return tag == c.tag;
	}

}