package myapp.java;

class CONSTANT_Double_Info
	extends CP_Info
{
	final
	double value;
	
	CONSTANT_Double_Info(double _value)
	{
		super(CP_Tag.CONSTANT_Double);
		value = _value;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_Double_Info c = (CONSTANT_Double_Info)o;
		return c.value == value;
	}
}
}