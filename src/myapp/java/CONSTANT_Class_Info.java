package myapp.java;

import java.io.DataOutputStream;;
import java.io.IOException;

import java.util.HashMap;

class CONSTANT_Class_Info
	extends CP_Info
{
	
	final
	CONSTANT_Utf8_Info name;
	
	CONSTANT_Class_Info( CONSTANT_Utf8_Info _name )
	{
		super( CP_Tag.CONSTANT_Class );
		name= _name;
	}
	
	void write( DataOutputStream dos , HashMap< CP_Info , Integer > pool ) throws IOException
	{
		super.write( dos , pool );
		dos.writeShort( pool.get( name ).shortValue() );
	}
		
	@Override
	public
	boolean equals( Object o )
	{
		if ( super.equals( o ) == false )
		{
			return false;
		}
		CONSTANT_Class_Info c = (CONSTANT_Class_Info)o;
		return name.equals( c.name );
	}
}