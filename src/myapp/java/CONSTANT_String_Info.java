package myapp.java;

class CONSTANT_String_Info extends CP_Info
{
	final int string_index;
	CONSTANT_String_Info(int _string_index)
	{
		super(CP_Tag.CONSTANT_String);
		string_index = _string_index;
	}
	
	
	@Override
	public boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_String_Info c = (CONSTANT_String_Info)o;
		return c.string_index == string_index;
	}
}