package myapp.java;

class CONSTANT_Float_Info
	extends CP_Info
{
	final
	float value;
	CONSTANT_Float_Info(float _value)
	{
		super(CP_Tag.CONSTANT_Float);
		value = _value;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_Float_Info c = (CONSTANT_Float_Info)o;
		return c.value == value;
	}
}