package myapp.java;

class CONSTANT_NameAndType_Info
	extends CP_Info
{
	
	final
	int name_index;
	
	final
	int descriptor_index;
	
	CONSTANT_NameAndType_Info(int _name_index, int _descriptor_index)
	{
		super(CP_Tag.CONSTANT_NameAndType);
		name_index = _name_index;
		descriptor_index = _descriptor_index;
	}
	
	@Override
	public
	boolean equals(Object o)
	{
		if (super.equals(o) == false)
		{
			return false;
		}
		CONSTANT_NameAndType_Info c = (CONSTANT_NameAndType_Info)o;
		return c.name_index == name_index && c.descriptor_index == descriptor_index;
	}
}