package myapp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import myapp.java.ClassFile;
import myapp.java.ClassAccessFlag;

public
class ClassBuilderTest
{
	
	public static
	void main( String[] args )
	{
		ClassAccessFlag[] flags = ClassAccessFlag.with( ClassAccessFlag.ACC_PUBLIC );
		ClassFile cf = new ClassFile( 7 , "myapp/MyClass" , flags , null , null );
		
		try ( ByteArrayOutputStream baos = new ByteArrayOutputStream() )
		{
			DataOutputStream dos = new DataOutputStream( baos );
			long v = 0xCAFEBABEL;
			dos.writeLong( v );
			
			int k = 0xCAFEBABE;
			
			dos.writeInt( k );
			
			byte[] b = baos.toByteArray();
			
			for ( byte i : b )
			{
				System.out.println(Integer.toString( 0xff & (int)i , 16 ) );
			}
			
		}
		catch ( IOException ex )
		{
			ex.printStackTrace();
		}
		
		
	}
	
}